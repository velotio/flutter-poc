import 'package:serverpod/serverpod.dart';

class ExampleFutureCall extends FutureCall {
  @override
  Future<void> invoke(Session session, SerializableEntity? object) async {}
}
