import 'package:serverpod_demo_server/src/generated/protocol.dart';
import 'package:serverpod/serverpod.dart';

class SessionEndpoint extends Endpoint {
  Future<Users?> login(Session session, String email, String password) async {
    List<Users> userList = await Users.find(session,
        where: (p0) =>
            (p0.email.equals(email)) & (p0.password.equals(password)));
    return userList.isEmpty ? null : userList[0];
  }

  Future<bool> signUp(Session session, Users newUser) async {
    try {
      await Users.insert(session, newUser);
      return true;
    } catch (e) {
      print(e.toString());
      return false;
    }
  }
}
