--
-- Class Users as table users
--

CREATE TABLE "users" (
  "id" serial,
  "username" text NOT NULL,
  "email" text NOT NULL,
  "password" text NOT NULL,
  "age" integer NOT NULL
);

ALTER TABLE ONLY "users"
  ADD CONSTRAINT users_pkey PRIMARY KEY (id);


