import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

class CustomAgeTextField extends StatelessWidget {
  final BoxConstraints constraints;
  final TextEditingController controller;
  final FocusNode node;
  final String label;

  const CustomAgeTextField(
      {super.key,
      required this.constraints,
      required this.controller,
      required this.node,
      required this.label});

  @override
  Widget build(BuildContext context) {
    return Center(
      child: ConstrainedBox(
        constraints: BoxConstraints(
            maxWidth: constraints.maxWidth > 600
                ? 600
                : MediaQuery.of(context).size.width),
        child: TextField(
          controller: controller,
          focusNode: node,
          keyboardType: TextInputType.number,
          inputFormatters: <TextInputFormatter>[
            FilteringTextInputFormatter.digitsOnly
          ],
          decoration: InputDecoration(
            labelText: label,
            border: const OutlineInputBorder(),
          ),
        ),
      ),
    );
  }
}
