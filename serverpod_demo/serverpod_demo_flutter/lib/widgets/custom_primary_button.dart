import 'package:flutter/material.dart';

class CustomPrimaryButton extends StatelessWidget {
  final BoxConstraints constraints;
  final VoidCallback onPressed;
  final String label;
  const CustomPrimaryButton(
      {super.key, required this.constraints, required this.onPressed, required this.label});

  @override
  Widget build(BuildContext context) {
    return Center(
      child: ConstrainedBox(
        constraints: BoxConstraints(
            minWidth: constraints.maxWidth > 600
                ? 400
                : MediaQuery.of(context).size.width,
            minHeight: 42.0),
        child: ElevatedButton(
          onPressed: onPressed,
          child: Text(label),
        ),
      ),
    );
  }
}
