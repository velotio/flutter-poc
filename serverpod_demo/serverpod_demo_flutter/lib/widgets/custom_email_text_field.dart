import 'package:flutter/material.dart';

class CustomEmailTextField extends StatelessWidget {
  final BoxConstraints constraints;
  final TextEditingController controller;
  final FocusNode node;
  final String label;

  const CustomEmailTextField(
      {super.key,
      required this.constraints,
      required this.controller,
      required this.node,
      required this.label});

  @override
  Widget build(BuildContext context) {
    return Center(
      child: ConstrainedBox(
        constraints: BoxConstraints(
            maxWidth: constraints.maxWidth > 600
                ? 600
                : MediaQuery.of(context).size.width),
        child: TextField(
          controller: controller,
          focusNode: node,
          keyboardType: TextInputType.emailAddress,
          decoration: InputDecoration(
            labelText: label,
            border: const OutlineInputBorder(),
          ),
        ),
      ),
    );
  }
}
