import 'package:flutter/material.dart';

class CustomErrorText extends StatelessWidget {
  final bool show;
  final String error;
  const CustomErrorText({super.key, required this.show, required this.error});

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        if (show) ...[
          const SizedBox(height: 32.0),
          Center(
            child: Text(
              error,
              style: const TextStyle(color: Colors.red),
            ),
          ),
        ],
      ],
    );
  }
}
