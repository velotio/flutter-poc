import 'package:flutter/material.dart';

class CustomPasswordTextField extends StatelessWidget {
  final BoxConstraints constraints;
  final TextEditingController controller;
  final FocusNode node;
  final String label;
  final bool show;
  final VoidCallback toggle;

  const CustomPasswordTextField(
      {super.key,
      required this.constraints,
      required this.controller,
      required this.node,
      required this.label,
      required this.show,
      required this.toggle});

  @override
  Widget build(BuildContext context) {
    return Center(
      child: ConstrainedBox(
        constraints: BoxConstraints(
            maxWidth: constraints.maxWidth > 600
                ? 600
                : MediaQuery.of(context).size.width),
        child: TextField(
          controller: controller,
          obscureText: !show,
          focusNode: node,
          keyboardType: TextInputType.visiblePassword,
          decoration: InputDecoration(
            labelText: label,
            border: const OutlineInputBorder(),
            suffixIcon: GestureDetector(
              onTap: toggle,
              child: Padding(
                padding: const EdgeInsets.all(16.0),
                child: Icon(
                  show ? Icons.visibility : Icons.visibility_off,
                  color: Colors.black,
                  size: 24,
                ),
              ),
            ),
          ),
        ),
      ),
    );
  }
}
