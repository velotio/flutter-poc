import 'package:flutter/material.dart';

class CustomUsernameTextField extends StatelessWidget {
  final BoxConstraints constraints;
  final TextEditingController controller;
  final FocusNode node;
  final String label;

  const CustomUsernameTextField(
      {super.key,
      required this.constraints,
      required this.controller,
      required this.node,
      required this.label});

  @override
  Widget build(BuildContext context) {
    return Center(
      child: ConstrainedBox(
        constraints: BoxConstraints(
            maxWidth: constraints.maxWidth > 600
                ? 600
                : MediaQuery.of(context).size.width),
        child: TextField(
          controller: controller,
          focusNode: node,
          keyboardType: TextInputType.name,
          decoration: InputDecoration(
            labelText: label,
            border: const OutlineInputBorder(),
          ),
        ),
      ),
    );
  }
}
