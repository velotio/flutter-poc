import 'package:serverpod_demo_flutter/pages/login_page.dart';
import 'package:flutter/material.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
        title: 'Session Demo - Serverpod',
        theme: ThemeData(primarySwatch: Colors.purple),
        home: const LoginPage());
  }
}
