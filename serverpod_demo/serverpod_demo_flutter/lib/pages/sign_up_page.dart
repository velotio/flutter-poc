// ignore_for_file: use_build_context_synchronously, implementation_imports

import 'package:serverpod_demo_flutter/config/utils.dart';
import 'package:serverpod_demo_flutter/widgets/custom_age_text_field.dart';
import 'package:serverpod_demo_flutter/widgets/custom_email_text_field.dart';
import 'package:serverpod_demo_flutter/widgets/custom_error_text.dart';
import 'package:serverpod_demo_flutter/widgets/custom_password_text_field.dart';
import 'package:serverpod_demo_flutter/widgets/custom_primary_button.dart';
import 'package:serverpod_demo_flutter/widgets/custom_username_text_field.dart';
import 'package:flutter/material.dart';
import 'package:serverpod_demo_client/src/protocol/users.dart';

class SignUpPage extends StatefulWidget {
  const SignUpPage({super.key});

  @override
  State<SignUpPage> createState() => _SignUpPageState();
}

class _SignUpPageState extends State<SignUpPage> {
  String _emailLabelText = 'Enter Email Address';
  String _usernameLabelText = 'Enter Username';
  String _passwordLabelText = 'Enter Password';
  String _ageLabelText = 'Enter Age';
  String _errorText = 'All field are mandatory !!!';

  final _emailEditingController = TextEditingController();
  final _usernameEditingController = TextEditingController();
  final _passwordEditingController = TextEditingController();
  final _ageEditingController = TextEditingController();

  final _emailFocusNode = FocusNode();
  final _usernameFocusNode = FocusNode();
  final _passwordFocusNode = FocusNode();
  final _ageFocusNode = FocusNode();

  bool _showPassword = false;
  bool _showError = false;

  @override
  void initState() {
    _emailFocusNode.addListener(() {
      setState(() {
        _showError = false;
        _emailFocusNode.hasFocus
            ? _emailLabelText = 'Email Address'
            : _emailEditingController.text.isEmpty
                ? _emailLabelText = 'Enter Email Address'
                : _emailLabelText = 'Email Address';
      });
    });

    _usernameFocusNode.addListener(() {
      setState(() {
        _showError = false;
        _usernameFocusNode.hasFocus
            ? _usernameLabelText = 'Username'
            : _usernameEditingController.text.isEmpty
                ? _usernameLabelText = 'Enter Username'
                : _usernameLabelText = 'Username';
      });
    });

    _passwordFocusNode.addListener(() {
      setState(() {
        _showError = false;
        _passwordFocusNode.hasFocus
            ? _passwordLabelText = 'Password'
            : _passwordEditingController.text.isEmpty
                ? _passwordLabelText = 'Enter Password'
                : _passwordLabelText = 'Password';
      });
    });

    _ageFocusNode.addListener(() {
      setState(() {
        _showError = false;
        _ageFocusNode.hasFocus
            ? _ageLabelText = 'Age'
            : _ageEditingController.text.isEmpty
                ? _ageLabelText = 'Enter Age'
                : _ageLabelText = 'Age';
      });
    });

    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return LayoutBuilder(builder: (context, constraints) {
      return Scaffold(
        appBar: AppBar(
          title: const Text('Sign Up'),
        ),
        body: Padding(
          padding: const EdgeInsets.all(16),
          child: Column(
            crossAxisAlignment: constraints.maxWidth > 600
                ? CrossAxisAlignment.center
                : CrossAxisAlignment.stretch,
            children: [
              CustomEmailTextField(
                constraints: constraints,
                controller: _emailEditingController,
                node: _emailFocusNode,
                label: _emailLabelText,
              ),
              const SizedBox(height: 16.0),
              CustomUsernameTextField(
                constraints: constraints,
                controller: _usernameEditingController,
                node: _usernameFocusNode,
                label: _usernameLabelText,
              ),
              const SizedBox(height: 16.0),
              CustomPasswordTextField(
                constraints: constraints,
                controller: _passwordEditingController,
                node: _passwordFocusNode,
                label: _passwordLabelText,
                show: _showPassword,
                toggle: togglePassVisibility,
              ),
              const SizedBox(height: 16.0),
              CustomAgeTextField(
                constraints: constraints,
                controller: _ageEditingController,
                node: _ageFocusNode,
                label: _ageLabelText,
              ),
              CustomErrorText(show: _showError, error: _errorText),
              const SizedBox(height: 32.0),
              CustomPrimaryButton(
                constraints: constraints,
                onPressed: signUpOnPressed,
                label: 'Sign Up',
              ),
            ],
          ),
        ),
      );
    });
  }

  void signUpOnPressed() async {
    if (_emailEditingController.text.trim().isEmpty ||
        _usernameEditingController.text.trim().isEmpty ||
        _passwordEditingController.text.trim().isEmpty ||
        _ageEditingController.text.trim().isEmpty) {
      setState(() {
        _errorText = 'All field are mandatory !!!';
        _showError = true;
      });
    } else {
      try {
        final result = await client.session.signUp(
          Users(
            email: _emailEditingController.text.trim(),
            username: _usernameEditingController.text.trim(),
            password: _passwordEditingController.text.trim(),
            age: int.parse(_ageEditingController.text.trim()),
          ),
        );
        if (result) {
          Navigator.pop(context);
        } else {
          setState(() {
            _errorText = 'Something went wrong, Try again.';
            _showError = true;
          });
        }
      } catch (e) {
        debugPrint(e.toString());
        setState(() {
          _errorText = e.toString();
          _showError = true;
        });
      }
    }
  }

  togglePassVisibility() {
    setState(() => _showPassword = !_showPassword);
  }
}
