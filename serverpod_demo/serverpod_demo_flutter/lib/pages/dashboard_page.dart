import 'package:flutter/material.dart';
import 'package:serverpod_demo_client/src/protocol/users.dart';

class DashboardPage extends StatefulWidget {
  final Users user;

  const DashboardPage({super.key, required this.user});

  @override
  State<DashboardPage> createState() => _DashboardPageState();
}

class _DashboardPageState extends State<DashboardPage> {
  @override
  Widget build(BuildContext context) {
    return LayoutBuilder(builder: (context, constraints) {
      return Scaffold(
        appBar: AppBar(
          title: const Text('Dashboard'),
        ),
        body: Padding(
          padding: const EdgeInsets.all(16),
          child: Center(
            child: ConstrainedBox(
              constraints: BoxConstraints(
                  maxWidth: constraints.maxWidth > 600
                      ? 600
                      : MediaQuery.of(context).size.width),
              child: Padding(
                padding: const EdgeInsets.all(16.0),
                child: Table(
                  border: TableBorder.all(),
                  children: [
                    TableRow(children: [
                      const Padding(
                          padding: EdgeInsets.all(8.0),
                          child: Text('Usename', textAlign: TextAlign.start)),
                      Padding(
                          padding: const EdgeInsets.all(8.0),
                          child: Text(widget.user.username,
                              textAlign: TextAlign.start))
                    ]),
                    TableRow(children: [
                      const Padding(
                          padding: EdgeInsets.all(8.0),
                          child: Text('Email', textAlign: TextAlign.start)),
                      Padding(
                          padding: const EdgeInsets.all(8.0),
                          child: Text(widget.user.email,
                              textAlign: TextAlign.start))
                    ]),
                    TableRow(children: [
                      const Padding(
                          padding: EdgeInsets.all(8.0),
                          child: Text('Age', textAlign: TextAlign.start)),
                      Padding(
                          padding: const EdgeInsets.all(8.0),
                          child: Text(widget.user.age.toString(),
                              textAlign: TextAlign.start))
                    ]),
                  ],
                ),
              ),
            ),
          ),
        ),
      );
    });
  }
}
