// ignore_for_file: use_build_context_synchronously

import 'package:serverpod_demo_flutter/pages/dashboard_page.dart';
import 'package:serverpod_demo_flutter/pages/sign_up_page.dart';
import 'package:serverpod_demo_flutter/config/utils.dart';
import 'package:serverpod_demo_flutter/widgets/custom_email_text_field.dart';
import 'package:serverpod_demo_flutter/widgets/custom_error_text.dart';
import 'package:serverpod_demo_flutter/widgets/custom_password_text_field.dart';
import 'package:serverpod_demo_flutter/widgets/custom_primary_button.dart';
import 'package:flutter/material.dart';

class LoginPage extends StatefulWidget {
  const LoginPage({Key? key}) : super(key: key);

  @override
  LoginPageState createState() => LoginPageState();
}

class LoginPageState extends State<LoginPage> {
  String _emailLabelText = 'Enter Email Address';
  String _passwordLabelText = 'Enter Password';
  String _errorText = 'All field are mandatory !!!';

  final _emailEditingController = TextEditingController();
  final _passwordEditingController = TextEditingController();

  final _emailFocusNode = FocusNode();
  final _passwordFocusNode = FocusNode();

  bool _showPassword = false;
  bool _showError = false;

  @override
  void initState() {
    _emailFocusNode.addListener(() {
      setState(() {
        _showError = false;
        _emailFocusNode.hasFocus
            ? _emailLabelText = 'Email Address'
            : _emailEditingController.text.isEmpty
                ? _emailLabelText = 'Enter Email Address'
                : _emailLabelText = 'Email Address';
      });
    });

    _passwordFocusNode.addListener(() {
      setState(() {
        _showError = false;
        _passwordFocusNode.hasFocus
            ? _passwordLabelText = 'Password'
            : _passwordEditingController.text.isEmpty
                ? _passwordLabelText = 'Enter Password'
                : _passwordLabelText = 'Password';
      });
    });
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return LayoutBuilder(builder: (context, constraints) {
      return Scaffold(
        appBar: AppBar(
          title: const Text('Login'),
        ),
        body: Padding(
          padding: const EdgeInsets.all(16),
          child: Column(
            crossAxisAlignment: constraints.maxWidth > 600
                ? CrossAxisAlignment.center
                : CrossAxisAlignment.stretch,
            children: [
              CustomEmailTextField(
                constraints: constraints,
                controller: _emailEditingController,
                node: _emailFocusNode,
                label: _emailLabelText,
              ),
              const SizedBox(height: 16.0),
              CustomPasswordTextField(
                constraints: constraints,
                controller: _passwordEditingController,
                node: _passwordFocusNode,
                label: _passwordLabelText,
                show: _showPassword,
                toggle: togglePassVisibility,
              ),
              CustomErrorText(show: _showError, error: _errorText),
              const SizedBox(height: 32.0),
              CustomPrimaryButton(
                constraints: constraints,
                onPressed: loginOnPressed,
                label: 'Login',
              ),
              const SizedBox(height: 16.0),
              CustomPrimaryButton(
                constraints: constraints,
                onPressed: signUpOnPressed,
                label: 'Sign Up',
              ),
            ],
          ),
        ),
      );
    });
  }

  void signUpOnPressed() {
    Navigator.push(
      context,
      MaterialPageRoute(
        builder: (context) => const SignUpPage(),
      ),
    );
  }

  void loginOnPressed() async {
    if (_emailEditingController.text.trim().isEmpty ||
        _passwordEditingController.text.trim().isEmpty) {
      setState(() {
        _errorText = 'All field are mandatory !!!';
        _showError = true;
      });
    } else {
      try {
        final result = await client.session.login(
          _emailEditingController.text.trim(),
          _passwordEditingController.text.trim(),
        );
        if (result != null) {
          _emailEditingController.text = '';
          _passwordEditingController.text = '';
          Navigator.push(
            context,
            MaterialPageRoute(
              builder: (context) => DashboardPage(user: result),
            ),
          );
        } else {
          setState(() {
            _errorText = 'Something went wrong, Try again.';
            _showError = true;
          });
        }
      } catch (e) {
        debugPrint(e.toString());
        setState(() {
          _errorText = e.toString();
          _showError = true;
        });
      }
    }
  }

  togglePassVisibility() {
    setState(() => _showPassword = !_showPassword);
  }
}
