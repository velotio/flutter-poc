package com.example.jnijen_hardware_details

import android.os.Build
import io.flutter.embedding.android.FlutterActivity

class MainActivity: FlutterActivity() {
    fun getHardwareDetails(): Map<String, String>? {
        val hardwareDetails: MutableMap<String, String> = HashMap()
        hardwareDetails["Language"] = "JAVA"
        hardwareDetails["Manufacture"] = Build.MANUFACTURER
        hardwareDetails["Model No."] = Build.MODEL
        hardwareDetails["Type"] = Build.TYPE
        hardwareDetails["User"] = Build.USER
        hardwareDetails["SDK"] = Build.VERSION.SDK
        hardwareDetails["Board"] = Build.BOARD
        hardwareDetails["Version Code"] = Build.VERSION.RELEASE
        return hardwareDetails
    }
}
