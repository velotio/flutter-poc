package com.hardware.utils;

import android.os.Build;

import java.util.HashMap;
import java.util.Map;

public class HardwareUtils {

    public Map<String, String> getHardwareDetails() {
        Map<String, String> hardwareDetails = new HashMap<String, String>();
        hardwareDetails.put("Manufacture", Build.MANUFACTURER);
        hardwareDetails.put("Model No.", Build.MODEL);
        hardwareDetails.put("Type", Build.TYPE);
        hardwareDetails.put("User", Build.USER);
        hardwareDetails.put("SDK", Build.VERSION.SDK);
        hardwareDetails.put("Board", Build.BOARD);
        hardwareDetails.put("Version Code", Build.VERSION.RELEASE);
        return hardwareDetails;
    }

    public Map<String, String> getHardwareDetailsKotlin() {
        return new HardwareUtilsKotlin().getHardwareDetails();
    }

}