package com.hardware.utils

import android.os.Build

class HardwareUtilsKotlin {

    fun getHardwareDetails(): Map<String, String>? {
        val hardwareDetails: MutableMap<String, String> = HashMap()
        hardwareDetails["Manufacture"] = Build.MANUFACTURER
        hardwareDetails["Model No."] = Build.MODEL
        hardwareDetails["Type"] = Build.TYPE
        hardwareDetails["User"] = Build.USER
        hardwareDetails["SDK"] = Build.VERSION.SDK
        hardwareDetails["Board"] = Build.BOARD
        hardwareDetails["Version Code"] = Build.VERSION.RELEASE
        return hardwareDetails
    }
}