import 'package:flutter/material.dart';
import 'package:jni/jni.dart';
import 'package:jnijen_hardware_details/hardware_utils.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({super.key});

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      home: const MyHomePage(title: 'Flutter Demo Home Page'),
    );
  }
}

class MyHomePage extends StatefulWidget {
  const MyHomePage({super.key, required this.title});

  final String title;

  @override
  State<MyHomePage> createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  String _hardwareDetails = '';
  String _hardwareDetailsKotlin = '';
  JObject activity = JObject.fromRef(Jni.getCurrentActivity());

  @override
  void initState() {
    JMap<JString, JString> deviceHardwareDetails =
        HardwareUtils().getHardwareDetails();
    _hardwareDetails = 'This device details from Java class:\n';
    deviceHardwareDetails.forEach((key, value) {
      _hardwareDetails =
          '$_hardwareDetails\n${key.toDartString()} is ${value.toDartString()}';
    });

    JMap<JString, JString> deviceHardwareDetailsKotlin =
        HardwareUtils().getHardwareDetailsKotlin();
    _hardwareDetailsKotlin = 'This device details from Kotlin class:\n';
    deviceHardwareDetailsKotlin.forEach((key, value) {
      _hardwareDetailsKotlin =
          '$_hardwareDetailsKotlin\n${key.toDartString()} is ${value.toDartString()}';
    });

    setState(() {
      _hardwareDetails;
      _hardwareDetailsKotlin;
    });
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(widget.title),
      ),
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            Text(
              _hardwareDetails,
              textAlign: TextAlign.center,
            ),
            SizedBox(height: 20,),
            Text(
              _hardwareDetailsKotlin,
              textAlign: TextAlign.center,
            ),
          ],
        ),
      ),
    );
  }
}
